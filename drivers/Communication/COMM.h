/*
 * COMM.h
 *
 *  Created on: 11/08/2016
 *      Author: Ray
 */

#ifndef COMM_H_
#define COMM_H_

//*****************************************************************************
//
//		Local Header Files
//
//*****************************************************************************

#include "Config.h"

//*****************************************************************************
//
//		Definitions
//
//*****************************************************************************
#define COMM_PORTS          8                               // System Frequency

//*****************************************************************************
//
//		Structures
//
//*****************************************************************************

//*****************************************************************************
//
//		Function Prototypes
//
//*****************************************************************************

void COMM_IntHandler0(void);
void COMM_IntHandler1(void);
void COMM_IntHandler5(void);
void COMM_IntHandler(uint8_t);
void COMM_Init(void);
void COMM_CommProcess(void);
void COMM_GetNextChar(uint8_t *ui8BufferCounter);
void COMM_SendFloat(float fNumber);

#endif /* COMM_H_ */
